# Final Exam Study Guide

This study guide is not necessarily comprehensive.  Anything covered in labs or in the prerequisite materials is also valid material for the exam.  However, the following material is guaranteed to be on the exam. Exam questions will include multiple choice, short answer, and code writing.

The Final Exam Date is
[Tuesday, April 30 10:30 - 12:30](https://www.tntech.edu/records/finalexamschedules.php).

## Topics

1. Git commands \& concepts
    - Commits
    - Branching
    - Push, pull, fetch
    - Basic git flow
    - Merges
2. Unix commands
    - Opening and editing files
    - Installing packages
    - Navigating files
    - Command line editors
    - Updating packages
    - Copying, renaming, and moving files
    - File and folder permissions
3. Containers, Docker commands, \& concepts
4. Infrastructure as code, Ansible commands, \& concepts
5. Markdown
6. ***Continuous Integration***
7. ***Continuous Delivery***
8. ***Continuous Deployment***

## Book Links

We have lost access to Safari Tech Books Online.  Use the below links to access the books required or recommended for this course.

- [Effective DevOps](http://ezproxy.tntech.edu/login?url=https://search.ebscohost.com/login.aspx?direct=true&scope=site&db=nlebk&db=nlabk&AN=1905988)
- [Linux Pocket Guide](http://ezproxy.tntech.edu/login?url=https://search.ebscohost.com/login.aspx?direct=true&scope=site&db=nlebk&db=nlabk&AN=1244943)
- [Jenkins 2: Up and Running](http://ezproxy.tntech.edu/login?url=https://search.ebscohost.com/login.aspx?direct=true&scope=site&db=nlebk&db=nlabk&AN=1802797)
- [AWS System Administration](http://ezproxy.tntech.edu/login?url=https://search.ebscohost.com/login.aspx?direct=true&scope=site&db=nlebk&db=nlabk&AN=1866533)