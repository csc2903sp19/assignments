# Lab 09 - Jenkins Initialization

## Due 3/26 @ Noon

***Remember to commit all changes with proper commit messages and to do so frequently, and to 
tag your final submission appropriately.***

## Preparatory Materials

### Read

- [Continuous Integration: What is CI?](https://codeship.com/continuous-integration-essentials)
- [Continusous Integration, explained](https://www.atlassian.com/continuous-delivery/continuous-integration)

### Reference

- [Jenkins](https://jenkins.io/)

## Instructions

1. Log in to [Jenkins](https://jenkins.csc.tntech.edu/) using your TTU credentials.
2. Create a "New Item".
3. Name the item `<unique_id> CSC 2903`, and be sure to replace `<unique_id>` with your username.
4. Select `Folder` then select `OK`.
5. Change the security settings so that you and I have permissions, but no one else does.
    - Check the box for `Enable project-based security`.
    - Change the inherit policy to not inherit.
    - `Add user or group`.
    - Type in my username, `rakaczkaje42`.
    - Grant me all of the permissions.
    - `Add user or group`.
    - Type in your username.
    - Grant yourself all the permissions.
    - Hit `Save`.
6. While in your new folder, add credentials.
    - Click `credentials`.
    - Click `Folder`.
    - Add a domain. Name this domain your `<unique_id>`.
    - Click on your new domain and then click `add credentials`.
    - Add `SSH Username with private key` credentials and add your private key directly.  This should be the same key that you use locally for GitLab. Be sure to provide your username and to give the credentials an ID of `<unique_id>-GitLab-SSH`.
7. While in your folder, creat a new item. Name your project `<unique_id> Demo` and  select `Freestyle Project` then select `OK`.
8. Connect the project to your repository.
    - Under `Source Code Management`, select `Git`.
    - For your `Repository URL` add the url for your repository using ssh.
    - Select your credentials we created earlier fromt the dropdown list.
    - Change `Branch Specifier` to be empty.
9. Tell Jenkins to build whenever you push a change.
    - Under `Build Triggers`, check `Build when a change is pushed to GitLab.` Copy the URL from this line and save it for later. It should be something like `http://jenkins.csc.tntech.edu:8080/project/project%20name`.
    - Enabled GitLab triggers should include `Push Events`, `Opened Merge Request Events`, `Approved Merge Requests`, and `Comments`.
    - Ensure that `Enable [ci-skip]`, `Ignore WIP Merge Requests`, and `Set build description...` checkboxes are checked under the `Advanced` options.
    - Generatea `Secret token` and copy it and save it for later. **You should have two items written down at this point.**
10. On your GitLab project, go to `Settings` > `Integrations`.
11. Add an integration for Jenkins by adding the URL and Secret Token from earlier.
12. Check the box for `Push Events`.
13. Save and test the integration.
14. On your Jenkins project you should see an event run.  The build should pass.
15. Write a blog post about what you've learned so far and submit your work with screenshot of your passing build.
