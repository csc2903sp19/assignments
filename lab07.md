# Lab 07 - Intro to Docker

## Due 2/21 @ Noon 
 
***Remember to commit all changes with proper commit messages and to do so frequently, and to 
tag your final submission appropriately.***

## Preparatory Materials

### Reference

- [Docker Docs](https://docs.docker.com/)

## Instructions

Today you will run through the basic Docker tutorial.

1. Run through the tutorial present [here](https://training.play-with-docker.com/beginner-linux/).
2. [Install Docker](https://docs.docker.com/install/linux/docker-ce/ubuntu/) locally.  You should also run the [post-installation](https://docs.docker.com/install/linux/linux-postinstall/) steps to run Docker without sudo.
3. Run an [nginx](https://hub.docker.com/_/nginx).  This [resource](https://blog.docker.com/2015/04/tips-for-deploying-nginx-official-image-with-docker/) may be useful in your deployment. 
4. Ensure the nginx Docker container is running and accessible from the browser.
5. Write blog post about what you've learned here.  Remember to tag your submission.
