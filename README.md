# Assignments

## About

This repository is for assignment sheet postings for CSC 2903 Special Topics (DevOps).

## Turn-in Instructions

Turning in assignments in GitLab requires the following:

- Consistent and regular commits.
- The final commit needs to be tagged with `Lab-XX-Final`, where `XX` is replaced with the lab number.