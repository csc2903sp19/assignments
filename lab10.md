# Lab 10 - Automatic Merging with Jenkins

## Due 3/28 @ Noon

## Instructions

1. On GitLab, add a branch called `jenkins-freestyle`.
2. Edit the Jenkins config to automatically merge master changes into the new branch.
    - On Jenkins, navigate to your project and click `configure`.
    - Find the `Source Code Management` section.
    - Click `Add` and then `Merge Before Build`.
    - Change `Name of repository` to `origin` and `Branch to merge to` to `jenkins-freestyle`.
    - Find the `Post-build Actions` section.
    - Click `Add post-build action` and `Git Publisher`.
    - Do the following:
        - Make sure `Push Only if Build Succeeds` and `Merge Results` are checked.
        - Change `Branch to push` to `jenkins-freestyle` and `Target remote name` to `origin`.
    - Hit `Save`.
3. Check to see that the added actions work by adding and commiting and pushing a change to your repository.  The Jenkins build should see the change, "build" the code, and then merge the changes into your new branch.
4. This is great, except what is our build doing here? Are we testing anything?  Under `Build Actions` is there anything that would be useful here?
5. Disable the build triggers in your initial project.
6. Managing our CI becomes easier if we use a `Multibranch Pipeline` instead of a `Freestyle Project`.
    - Create a `Multibranch Pipeline` item in your folder.
    - Under `Branch Sources` add your repository and credentials.
    - Under `Branch Sources`, add the following behaviors.
        - Check out to matching local branch
        - Clean after checkout
        - Clean before checkout
    - Select `save`.
7. Manually run the pipeline from the Jenkins interface and ensure that the run is successful.
8. Next time we'll start writing a [Jenkinsfile](https://jenkins.io/doc/book/pipeline/jenkinsfile/), which will instruct the pipeline what to do at each stage.
9. Write a short blog post about what you've learned so far.