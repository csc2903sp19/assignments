# Lab 11 - Jenkins and Multibranch Pipelines

## Due 4/16 @ Noon

## Instructions

___
### Part 1

1. Connect your multibranch Jenkins project to GitLab.
    - `Configure` the Jenkins project to `Scan for Pipeline Triggers'.
    - On GitLab add a webhook for push events.
    - Copy the URL of a previous webhook, changing the last portion of it to be the name of your project (replace any spaces with `%20`).
    - Add a Jenkinsfile to your repository.
    - In your Jenkinsfile, add the following:
    ```groovy
    pipeline {
        agent {
            docker {image 'node:7-alpine'}
        }
        post {
          failure {
            updateGitlabCommitStatus name: 'build', state: 'failed'
          }
          success {
            updateGitlabCommitStatus name: 'build', state: 'success'
          }
        }
        options {
          gitLabConnection('GitLab')
        }
        triggers {
            gitlab(triggerOnPush: true, triggerOnMergeRequest: true, branchFilterType: 'All')
        }
        stages {
            stage('Test') {
                steps {
                    sh 'node --version'
                }
            }
        }
    }
    ```
    - Push the change to your repository.
    - Manually run the pipeline.
    - After it runs, test the GitLab connection we added earlier.
    - You should have a successful run on push events and manual runs.
2. Make note of what this Jenkinsfile is doing.
    - What is agent?
    - What are stages?
    - What does post and triggers do?
3. Investigate the build log.  How does the build log match up to the Jenkinsfile?
4. Change the Jenkinsfile to work with _our_ project and _our_ Dockerfile.
    - Change `agent` to `none` without brackets.
    - Add this line `agent {dockerfile { args '-u root --privileged'}}
` line in the test stage. 
    - Add steps in the test stage to change to your blog directory, complete a bundle install, and run rspec tests.
    - In your repository, copy your `Dockerfile.dev` to a file named `Dockerfile`.
    - Commit and push your changes.
5. At first your build may fail. If this occurs, we need to modify our rpsec helper to work within this environment.
    - Change the following
    ```ruby
    chromeOptions: {
        args: %w[headless disable-gpu]
    }
    ```
    to:
    ```ruby
    chromeOptions: {
        args: %w[headless disable-gpu no-sandbox]
    }
    ```
6. At this point, your build should be passing. Write a blog post about what you've learned.

___
### Part 2

You may have noticed that Jenkins feels a bit clunky to navigate and it can be difficult to find the information you need.  Today you'll be introduced to a new feature of Jenkins called Blue Ocean. We will also add a build stage to our pipeline.

1. On the homepage of Jenkins, find the `Open Blue Ocean` link. Navigate to your pipeline on the Blue Ocean interface.
2. Look around.  Is there any information easier or harder to find? Is there anything here that you couldn't find in the previous interface?
3. Can you add a new pipeline in Blue Ocean? How does the process differ? If you create any new pipelines, be sure to delete them when you are done.  However, feel free to expierement and test out Blue Ocean's features.
4. From this point on, you can use the Blue Ocean interface or the Jenkins default interface for interacting with your pipeline.
5. Let's add a build stage to our pipeline.
    - Add a stage called `Build` after our testing stage.
    - Give it a Docker agent with an image of `jekyll/jekyll`. DO NOT GIVE IT ROOT.
    - ~~Add the following to the agent as well: <br>`args '-v ${WORKSPACE}/blog:/srv/jekyll'`~~
    - Be sure to change the path from `blog` to the correct path for just your blog folder in your repository. ~~(assume `${WORKSPACE}` starts in the root of your repository.)~~
    - Add steps to do the following:
        - Change to your `blog` directory.
        - Do a bundle install,
        - Do a jekyll clean.
        - Do a jekyll build.
6. Building the site is only so useful however, as we have no way to access it from Jenkins.  This is where _archiving artifacts_ becomes useful.  
    - Add a step to create a tarball of your `_site` directory. The command for this should look like `tar -cvf <tar-file>.tar <folder>`
    - Add an archiveArtifacts step to archive your `<tar-file>.tar` file. Use Jenkins documentation to help you with this.
7. At this point, your pipeline should run succesfully, and you should have an accessible artifact from Jenkins.
8. Write a blog post about what you've learned today.
