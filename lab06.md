# Lab 06 - Automation VM Configuration

## Due 2/19 @ Noon

***Remember to commit all changes with proper commit messages and to do so frequently, and to 
tag your final submission appropriately.***

## Preparatory Materials

None.

## Instructions

***Remember to update your `hosts.yml` file for your new AWS instance.***

1. Create a new AWS instance (following the same procedure from Lab 04).
2. Test your ssh connection to the server.
3. Create a new branch called `lab06` and work in it.
4. Write an Ansible playbook called `blog-install.yml` to do the following:
    - Install nginx.
    - Copy your site files to the correct directory.
5. Screenshot the working site and the console output from your ansible playbook and include them in your blog post.
6. Write a blog post about your experiences with Ansible this week.
7. Merge the branch and submit your work.
