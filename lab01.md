# Lab 01 - Working with a Repository

## Due 1/24 @ Noon

This lab will demonstrate your ability to utilize git with ssh and set up a repository. Important commands for this lab include `init`, `commit`, `push`, and `pull`. Keep in mind every change to your repository should be followed by an appropriate commit to your repository with a meaningful commit message. It is expected that you have completed the git tutorial from Labb 00 before begining this lab as well as watched the relevant videos on Jekyll and Markdown.

Your final submission of code should be _tagged_ with `Lab-01-Final` by noon on Thursday (January 24th).

## Preparatory Materials

### Read

- [An Introduction to Static Site Generators](https://davidwalsh.name/introduction-static-site-generators)
- [Why use a Static Site Generator?](https://learn.cloudcannon.com/jekyll/why-use-a-static-site-generator/)
- [Jekyll](https://jekyllrb.com/)
- [What is Markdown?](http://kirkstrobeck.github.io/whatismarkdown.com/)

### Reference

- [Markdown Guide](https://www.markdownguide.org/)
- [Explain Shell](https://explainshell.com/)
- [Linux Cheat Sheet](https://ryanstutorials.net/linuxtutorial/cheatsheet.php)
- [Git Cheat Sheet](https://about.gitlab.com/images/press/git-cheat-sheet.pdf)

### Tutorial

- [CLI Tutorial](https://www.codecademy.com/learn/learn-the-command-line)
- [Git Tutorial](https://www.codecademy.com/learn/learn-git)
- [Linux Tutorial](https://ryanstutorials.net/linuxtutorial/)

## Instructions

1. Create a private repository on [GitLab](https://gitlab.csc.tntech.edu).  Use your student email. Share this repository with my tech email. This is where you will submit your work.
2. Set up an [ssh key for your repository](https://docs.gitlab.com/ee/ssh/). Use ssh whenever you interact with your repository.
3. On your local machine, Create a folder either on your Desktop or in your Home directory called `CSC 2903 Repo` and run `git init` to initialize the git repository.
4. Commit a `README.md` with the following:
    - A level 1 heading that includes your name
    - A level 2 heading called `About`
    - A description of this project underneath the `About` header
    - Keep this readme updated throughout the course with items such as how to build, run, and test the project and other pertinent information.
5. Create a folder within `CSC 2903 Repo` called `blog`.
6. In `CSC 2903 Repo/blog/`, [Install](https://jekyllrb.com/docs/installation/) & [create](https://jekyllrb.com/docs/) a Jekyll site locally.
7. Run and test the site.
8. Commit the site your TTU GitLab Repository.
9. Every change you make should have its own commit in the repository. Make the following changes to your site:
    - Change the title
    - Change the Footer to include your student email
    - Remove the GitHub and Twitter links
    - Edit the site description
    - Update the `About` page to be about you.
    - Remove the default post and [add a post](https://jekyllrb.com/docs/posts/) about your expectations for this class.
