# Lab 04 - Manual VM Provisioning and Configuration

## Due 2/5 @ Noon

***Remember to commit all changes with proper commit messages and to do so frequently, and to tag your final submission appropriately.***

## Preparatory Materials

No preparatory materials are needed for this lab.

## Instructions

1. Login to [AWS Educate](https://aws.amazon.com/education/awseducate/) and navigate to your classroom for this course.
2. Click on `AWS Console`.
3. Under `Build a solution` click `Launch a virtual machine`.
4. Find the Ubuntu server and select it.
5. Keep the `t2.micro` and select `Next: Configure Instance Details`.
6. Click `Next: Add Storage`.
7. Click `Next: Add Tags`.
8. Click `Next: Configure Security Group`.
9. Click `Add Rule` and add two rules; `HTTP` and `HTTPS`. And change `source` to `anywhere`.
10. Then click `Review and Launch`.
11. Then click `Launch`.
12. You'll be presented with a dialog window asking about key pairs.  Create a new one. Give it a name of `AWS-CSC2903`, and download the file. Put the file in your `~/.ssh` directory.
13. Click `Launch Instance`.
14. Click `View Instances`.
15. Click on your running instance.
16. Use your terminal to ssh into the Public IP or Public DNS address of your running instance using the ssh key you downloaded.
17. Run an `apt update` and `apt upgrade`, then `reboot`.
18. SSH back into the machine.
19. Install `nginx`. Nginx is a webserver and we'll be using it to host our application.
20. Go to your instance in the browser. Confirm that you can see the nginx default page.
21. Replace the contents of `/var/www/html` with the contents from your `_site` folder from inside the `blog` directory. 
    You can do this using sftp commands. ~~Use [this](https://superuser.com/questions/1345454/sftp-connection-with-ssh-key-on-ubuntu) to help you set up sftp with ssh.~~ Use `sftp -i "<location of key file>" ubuntu@<your ip>`
    - This directory doesn't always exist.  If you don't see it in your directory run the following:
        - `bundle exec jekyll clean`
        - `bundle exec jekyll build`
22. Check to see that your AWS instance now shows your site. Demo it to the instructor. Screenshot the browser window and include its image in the post you write in the next step.
23. Stop your instance and write a post describing what you've done in this lab.