# Lab 00 "Git-ing" Prepared

This lab is ungraded but it is imperative that you complete it in order to have successful labs in the future.

## Learning Git

Complete the interactive git tutorial [here](https://www.katacoda.com/courses/git). Knowing how to utilize git will be important for Lab 01.

## Install a VM

Use VirtualBox to install an Ubuntu 18.04 VM. Please ensure that you install a version with a GUI, or install a GUI manually. Install Git on this VM.
Instead, download the [Ubuntu 18 Desktop iso file](https://www.ubuntu.com/#download) and use the instructions found [here](https://linuxhint.com/install_ubuntu_18-04_virtualbox/).
__Note if you are already running an Ubuntu 16+ OS on your device, you may skip this step.__

___All work for this course is expected to be completed using Ubuntu.  Some of the tools we will require are not present on other operating systems.___

## Additional Resources

If you would like more Git tutorials, or need more help seeting up VirtualBox and your Ubuntu VM, you can use the resource document found [here](http://bit.ly/csc-se-resources).
__Note that you should not use the VirtualBox instruction included in the document, except for VirtualBox's installation.__  