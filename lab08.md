# Lab 08 - Docker and Dockerfiles

## Due 2/28 @ Midnight

***Remember to commit all changes with proper commit messages and to do so frequently, and to 
tag your final submission appropriately.***

## Preparatory Materials

### Read

- [Best Practices](https://docs.docker.com/develop/develop-images/dockerfile_best-practices/)
- [Dockerfile Documentation](https://docs.docker.com/engine/reference/builder/)


## Instructions

**All files for this lab should be placed in the top level of your repository's directory.  For the file created during the tutorial, call it `Dockerfile.prod`, for the file created after the tutorial call it `Dockerfile.dev`.  When you build your command will look something like `docker build Dockerfile.prod .`**

When running your container in interactive mode make sure to use `-it`; `i` tells it `interactive` and `t` gives you the command prompt.

1. ~~Create a folder in your repository `Docker-scripts`.~~
2. Follow the tutorial shown [here](https://rominirani.com/docker-tutorial-series-writing-a-dockerfile-ce5746617cd). IGNORE ANYTHING THAT SAYS `boot-2-docker`. ~~Place all files in the `Docker-scripts/production`.~~ Name this file `Dockerfile.prod`. After completing the tutorial do the following:
    - Do not create the `images` folder.
    - Make sure the `MAINTAINER` is changed to your name.
    - Change the `COPY` to copy your `_site` files.
    - Test to make sure that this image correctly shows your blog site.

3. ~~Create a new folder in `Docker-scripts` called `development`.~~
4. ~~Create a Dockerfile in `Docker-scripts/development`.~~  Use this `Dockerfile.dev` to create a Docker image to create a development container. It should use the following:
    - An ubuntu base
    - Ruby and Jekyll
        - If you are having trouble with the `soure ~./bashrc` command use [this](https://stackoverflow.com/questions/20635472/using-the-run-instruction-in-a-dockerfile-with-source-does-not-work) to help you.
    - Capybara and related dependencies
    - ALL of your Jekyll files
    - git
5.  Test that this Dockerfile creates a container that works as intended (a development environment that behaves like your `blog` folder).
6. Write a blog post describing your process and what you've learned.
