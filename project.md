# Project

## Due 4/26 @ Midnight

***Remember to commit all changes with proper commit messages and to do so frequently, and to tag your final submission appropriately.***

## Preparatory Materials

### Read

- [GitLab Pages](https://about.gitlab.com/product/pages/)

### Reference

- [GitLab Pages Guide](https://docs.gitlab.com/ee/user/project/pages/getting_started_part_two.html)
- [GitLab CI](https://gitlab.com/help/ci/yaml/README)

## Instructions

1. Use the above links to create a GitLab page for yourself on the ***public*** GitLab, though your project itself should be _private_. Be sure to add me as a member with owner or maintainer permissions (username: rakaczkaje).  ***You must create a project from scratch. Do not fork, copy, or clone an existing example project.***  You may however pick any static site generator supported by GitLab.

2. Use this gitlab page to create a simple portfolio site for either yourself or one of your favorite fictional characters.  Feel free to change themes, site structure, and content.  Minimal requirements are as follows:
    - Name
    - Contact Info
    - An 'About Me' page or section
    - A profile picture

3. When your page is completed, create a blog post in your course repository documenting your experience, what you've learned, and links to both your repository and GitLab site.
