# Midterm Study Guide

This study guide is not necessarily comprehensive.  Anything covered in labs or in the prerequisite materials is also valid material for the exam.  However, the following material is guaranteed to be on the exam. Exam questions will include multiple choice, short answer, code writing and re

1. Git commands \& concepts
    - Commits
    - Branching
    - Push, pull, fetch
    - Basic git flow
	- Merges
2. Unix commands
    - Opening and editing files
    - Installing packages
    - Navigating files
    - Command line editors
    - Updating packages
    - Copying, renaming, and moving files
    - File and folder permissions
3. Containers, Docker commands, \& concepts
4. Infrastructure as code, Ansible commands, \& concepts
5. Markdown
