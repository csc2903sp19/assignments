# Lab 03 - Git Branching and Merging

## Due 1/31 @ Noon

***Remember to commit all changes with proper commit messages and to do so frequently, and to tag your final submission appropriately.***

## Preparatory Materials

### Read

- [Atlassian: Branches](https://www.atlassian.com/git/tutorials/using-branches)
- [Atlassian: Merge](https://www.atlassian.com/git/tutorials/using-branches/git-merge)
- [Digital Ocean: How to Use Git Branches](https://www.digitalocean.com/community/tutorials/how-to-use-git-branches)

### Reference

- [Git: Flight Rules](https://github.com/k88hudson/git-flight-rules)

### Tutorials

- [GitHug: Git Tutorial](https://github.com/Gazler/githug)

### Humor

- [XKCD on Git](https://xkcd.com/1597/)

## Instructions

Document your answers to document your experiences in during this lab.  You will write a blog post at the end of this lab explaining what you've learned.

### Set Up

1. Create 2 folders `Alice` and `Bob`.  **Neither folder should be in your project folder for this course.**  They should be in your home directory, desktop, or documents folder. Just do not put them in the same folder as your repository.
2. Using your url from GitLab, clone your project twice. Once to`Alice` and once to `Bob`. Keep a terminal window open in both. Your directory structure should look something like this:
    ```bash
    ├── Alice
    │   └── blog
    ├── Bob
    │   └── blog
    └── CSC2903
        └── blog

    ```
3. Run the following in each directory:
    - `git pull`
    - `git push`
    - `git log`
    - `git log --all --graph --decorate --oneline`
    - `git branch -a -v`

   The results of each should be the same.

### Merging Without Branching

4. In Alice's folder:
    - Create a folder called `lab-03`.
    - Create a file in the folder called `cat.txt`.
    - Put whatever text you like in its contents (it can be as simple as `Meow`).
    - Add, commit, and push the changes.
5. In Bob's folder:
    - **DO NOT PULL.**
    - Create a folder called `lab-03`.
    - Create a file called `dog.txt`.
    - Put whatever you like in its contents.
    - Add and commit the file, but do not push it.
    - Run `git log --all --graph --decorate --oneline`. Make sure you understand the output.
    - Run `git pull`.
    - Run the git log command again.
    - Check to see what files are present in the folder you created.
    - Push your changes.
6. In Alice's folder:
    - Run `git pull`.
    - Note what happens.
    - Run the git log command again, and compare it to Bob's.
7. What happened between the two folders?

### Git Checkout

8. In Alice's folder:
    - Identify where `HEAD` is using `git log`.
    - Do a `git log` and identify the *commit hash* from just before you andded the `cat.txt` and `dog.txt` files.
    - Run `git checkout <your-hash>`.
    - Note the contents of the repository.
    - Identify where `HEAD` is now.
    - Can you move `HEAD` to the commit where `dog.txt` was created? Is `dog.txt` there? Is `cat.txt` there? Why or why not?
    - Checkout master.
    - Where is `HEAD`? What do your files look like? What does `git checkout` do?

### Merge Conflicts

9. Ensure Alice's and Bob's repositories have the same state.
10. In Alice's folder:
    - Change the first line of `cats.txt` to `Alice likes cats`.
    - Add, commit, and push the changes.
11. In Bob's folder:
    - **Do not pull**.
    - Change the first line of `cats.txt` to `Bob likes cats`.
    - Commit the changes, then pull.
    - What happened? Resolve the issue. And ensure that your files are in your desired state.
    - Commit and push your changes.
12. In Alice's folder, pull the changes.  Ensure that Alice's and Bob's files are in the same state.

### Branching

13. Run `git branch -a`, `git branch -v`, and `git branch -a -v` and note what they do.
14. In Alice's folder:
    - Create a new branch: `git branch goat`.
    - Identify where `HEAD` is.  Has anything changed?
    - Run `branch -a -v`.  Has anything changed?
    - Use git checkout to switch to the new branch. 
    - Identify where `HEAD` is. Has anything changed?
    - Create a file in your `lab03` folder called `goat.txt` and add text to it.
    - Add and commit the changes.  Push the changes using `git push -u origin goat`
    - Agian, identify where `HEAD` is located. List branches as well.  Note the results.
15. In Bob's folder:
    - List the branches and locate `HEAD`.
    - Run a git pull.
    - List the branches and locate `HEAD`.
    - Note the contents of the repository.
    - Create a file in the `lab03` folder called `moose.txt`.
    - Add, commit, and push the changes.
    - Checkout the `goat` branch.
    - Locate `HEAD` and note the contents of the directory.
16. In Alice's folder:
    - Run a pull.
    - Look at the commit graph (the long `git log` command), locate the `HEAD`, and note the contents of the repository. 
    - Do you see all the files you created?

### Merging a Branch

17. In both Alice's and Bob's folder:
    - Run a pull and checkout master.
    - Look at the commit graph and the branches listed.
18. In Alice's folder:
    - Run git merge goat, to merge goat into master.
    - Look at the commit graph again, and note the files in the repository.
    - Push the changes.
19. In Bob's folder:
    - Look at the branches and commit graph.
    - Run a pull.
    - Look at the branches and commit graph again. Note the differences.

### Branch for Post

20. Create a new branch and work in this branch to create a new post.  In this post document your experiences with this lab and what you've learned.
21. Merge the branch into master and tag your submission.

### Notes

Branches can also be deleted. But is generally not recommended. However, it is useful to understand and know the commands.  It is recommended to research this information.
