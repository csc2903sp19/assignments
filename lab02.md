# Lab 02 - Testing

## Due 1/29 @ Noon

Test Driven Development or *TDD* is important to good software engineering practices. Tests are also an important part of DevOps. Typically, you should write your tests *before* you write your code. We'll begin by writing basic rspec tests to verify the existence of pages.

***Remember to commit all changes with proper commit messages and to do so frequently.***

## Preparatory Materials

### Read

- [Software Testing](https://www.tutorialspoint.com/software_testing/software_testing_quick_guide.htm)
- [Atlassian Software Testing](https://www.atlassian.com/continuous-delivery/software-testing)
- [Atlassian Software Testing: Types](https://www.atlassian.com/continuous-delivery/software-testing/types-of-software-testing)
- [Atlassian Software Testing: Coverage](https://www.atlassian.com/continuous-delivery/software-testing/code-coverage)
- [Capybara](https://www.sitepoint.com/basics-capybara-improving-tests/)

### Reference

- [Capybara GitHub](https://github.com/teamcapybara/capybara)
- [Capybara Cheat Sheet](https://devhints.io/capybara)

## Instructions

1. First, we'll need to add certain gems to aid with testing.

    - In your `Gemfile`, add a group for `development` and `test` (this should be a single group). Use the [documentation](https://bundler.io/v2.0/guides/groups.html) to aid this task.
    - Add the following gems:
        - rspec
        - selenium-webdriver
        - webdrivers
        - capybara
        - rack-jekyll
        - puma
    - Run `bundle install` to install the gems. You may need to install some missing libraries to get this to work properly.
2. Install chromedriver.
    3. Use [this](https://askubuntu.com/questions/1004947/how-do-i-use-the-chrome-driver-in-ubuntu-16-04) to help you.
3. Next, init rspec. This can be done with the `rspec --init` command. This should create two files: `.rspec` and `spec/spec_helper.rb`.
4. Replace the contents of `spec/spec_helper.rb` file with the following:
    ```ruby
    # Require all of the necessary gems
    require 'rspec'
    require 'capybara/rspec'
    require 'rack/jekyll'
    require 'rack/test'
    require 'webdrivers'
    require 'selenium/webdriver'

    RSpec.configure do |config|
    config.expect_with :rspec do |expectations|
        expectations.include_chain_clauses_in_custom_matcher_descriptions = true
    end

    config.mock_with :rspec do |mocks|
        mocks.verify_partial_doubles = true
    end

    # Configure Capybara to use Selenium.
    Capybara.register_driver :selenium do |app|
        # Configure selenium to use Chrome.
        Capybara::Selenium::Driver.new(app, :browser => :chrome)
    end

    # Configure Capybara to load the website through rack-jekyll.
    # (force_build: true) builds the site before the tests are run,
    # so our tests are always running against the latest version
    # of our jekyll site.
    Capybara.app = Rack::Jekyll.new(force_build: true)
    end
    ```
4. Now, add the first test. Replace `'Welcome to Jekyll!'` with your first posts name.
    - Create `spec/sample_spec.rb`.
    - Add the following to the file:
    ```ruby
    describe "sample", type: :feature, js: true do
        it "has the page title" do
            visit '/'
            expect(find('.post-link').text).to eq('Welcome to Jekyll!')
        end
    end
    ```
5. Run the test using `bundle exec rspec`.
    - The test should open and close a browser in order to perform the tests.
6. Opening a browser for testing is fine for systems with a GUI but for automated testing, GUIs are often not available.  We need to change the browser to a *headless* browser.
    - Replace the `Capybara.register_driver` section of code `spec_helper.rb` with the following:
        ``` ruby
        Capybara.register_driver(:headless_chrome) do |app|
            capabilities = Selenium::WebDriver::Remote::Capabilities.chrome(
                chromeOptions: { args: %w[headless disable-gpu] }
            )

            Capybara::Selenium::Driver.new(
                app,
                browser: :chrome,
                desired_capabilities: capabilities
            )
        end

        Capybara.javascript_driver = :headless_chrome
        ```
7. Run the tests again.  This time no browser should appear.
8. Write a blog post documenting your process.
9. For the following use the [Capybara Cheatsheet](https://devhints.io/capybara), [RSpec](https://devhints.io/rspec), and [Capybara Tutorial](https://www.sitepoint.com/basics-capybara-improving-tests/) to write your tests (you can mostly skip the sections on setup and installation).
    - Write your own tests to test the existence of each post on your site and that they have the correct post title. Put these new tests in their own file entitled `posts_spec.rb`. You can remove the `sample_spec.rb` file at this point.
    - Write tests to confirm that you've changed the email address and place them in a file called `footer_spec.rb`.
